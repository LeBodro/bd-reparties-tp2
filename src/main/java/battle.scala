import fighters.Fighter
import org.apache.spark.graphx.{Edge, EdgeContext, Graph}
import org.apache.spark.rdd.RDD

case class Node(fighter: Fighter, hp: Int, targetId: Long) extends Serializable

class Battle() extends Serializable {
  private var fighters: Vector[(Long, Node)] = Vector[(Long, Node)]()
  private var currentIndex = 1L
  private var graph: Graph[Node, String] = _
  private val random = scala.util.Random

  def AddFighter(fighter: Fighter): Unit = {
    fighters = fighters :+ (currentIndex, Node(fighter, fighter.hitPointsMax, -1L))
    currentIndex = currentIndex + 1L
  }

  def GetVerticesAndEdges(): (Vector[(Long, Node)], Vector[Edge[String]]) = {
    var edges: Vector[Edge[String]] = Vector[Edge[String]]()

    for (i <- 0 until fighters.size - 1) {
      for (j <- i + 1 until fighters.size) {
        val relationship: String = if (fighters(i)._2.fighter.team.contentEquals(fighters(j)._2.fighter.team)) "Friendly" else "Hostile"
        edges = edges :+ Edge[String](fighters(i)._1, fighters(j)._1, relationship)
      }
    }
    (fighters, edges)
  }

  def RollInitiative(frdd: RDD[(Long, Node)], erdd:RDD[Edge[String]]): Unit = {
    graph = Graph[Node, String](frdd, erdd)
  }

  // Fight
  private def ResolveAttack(attacker: Fighter, victim: Fighter): Int = {
    val damages: List[Int] = for (attack <- attacker.attacks) yield attack.RollAgainst(victim.armorClass)
    val totalDamage = damages.sum
    if(totalDamage > 0)
      println("%s dealt %d of damage to %s!".format(attacker.name, totalDamage, victim.name))
    else
      println("%s attacked %s and missed.".format(attacker.name, victim.name))
    totalDamage
  }

  private def BothAreAlive(triplet: EdgeContext[Node, String, Long]): Boolean = {
    triplet.srcAttr.hp > 0 && triplet.dstAttr.hp > 0
  }

  private def TheyAreEnemies(triplet: EdgeContext[Node, String, Long]): Boolean = {
    !triplet.srcAttr.fighter.team.contentEquals(triplet.dstAttr.fighter.team)
  }

  def PlayRound(): Boolean = {
    val attackTargets = graph.aggregateMessages(
      (triplet: EdgeContext[Node, String, Long]) => {
        // If not on the same team, fighters will want to fight each other.
        if (BothAreAlive(triplet) && TheyAreEnemies(triplet)) {
          triplet.sendToDst(triplet.srcId)
          triplet.sendToSrc(triplet.dstId)
        }
      },
      (victim1: Long, victim2: Long) => {
        // Randomly chose who to attack.
        if (random.nextInt(2) == 0)
          victim1
        else
          victim2
      }
    )

    graph = graph.joinVertices(attackTargets)((id, attacker, victim) => {
      Node(attacker.fighter, attacker.hp, victim)
    })

    val damageResolution = graph.aggregateMessages(
      (triplet: EdgeContext[Node, String, Int]) => {
        if (triplet.srcAttr.targetId == triplet.dstId) {
          triplet.sendToDst(ResolveAttack(triplet.srcAttr.fighter, triplet.dstAttr.fighter))
        }
        if (triplet.dstAttr.targetId == triplet.srcId) {
          triplet.sendToSrc(ResolveAttack(triplet.dstAttr.fighter, triplet.srcAttr.fighter))
        }
      },
      (damage1: Int, damage2: Int) => {
        damage1 + damage2
      }
    )

    graph = graph.joinVertices(damageResolution)((id, old, damage) => {
      val remainingHp = old.hp - damage
      if(old.hp > 0 && remainingHp <= 0)
        println("%s bites the dust...".format(old.fighter.name))
      else
        println("%s : %d/%d".format(old.fighter.name, remainingHp, old.fighter.hitPointsMax))
      Node(old.fighter, old.hp - damage, -1L)
    })

    attackTargets.count() > 0
  }
}