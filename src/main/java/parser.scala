package parser

import scala.util.parsing.json.JSON

case class Creature(name : String, spells : List[String]) extends Serializable

class ClassCast[T] { def unapply(a:Any):Option[T] = Some(a.asInstanceOf[T]) }

object jMap extends ClassCast[Map[String, Any]]
object jList extends ClassCast[List[Any]]
object jStringList extends ClassCast[List[String]]
object jString extends ClassCast[String]
object jInt extends ClassCast[Int]

class DNDJsonParser(val json: String) {
  def ParseSpellCasters(): List[Creature] = {
    for {
      Some(jList(creatures)) <- List(JSON.parseFull(json))
      jMap(creature) <- creatures
      jString(name) = creature("name")
      jStringList(spells) = creature("spells")
    } yield Creature(name, spells)
  }

  /*def ParseFighters(): List[Fighter] = {
    for {
      Some(jList(fighters)) <- List(JSON.parseFull(json))
      jMap(fighter) <- fighters
      jString(name) = fighter("name")
      jString(team) = fighter("team")
      jInt(ac) = fighter("armorClass")
      jInt(maxHP) = fighter("hitPointsMaximum")
      jStringList(attacks) = fighter("attacks") // Attack format example: 2d6+4
    } yield Fighter(name, team, ac, maxHP, attacks)
  }*/
}