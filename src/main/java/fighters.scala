package fighters

import scala.collection.mutable.ArrayBuffer

object Bestiary {
  val enemyTeam = "Enemy"
  val allyTeam = "Ally"

  def Solar(): Fighter = {
    val damage = Dice(3, 6, 18)
    Fighter(
      name="Solar",
      team=allyTeam,
      armorClass=44,
      hitPointsMax=Dice(22, 10, 242).Roll(),
      attacks=List[Attack](
        Attack(35, 10, damage),
        Attack(30, 10, damage),
        Attack(25, 10, damage),
        Attack(20, 10, damage)
      )
    )
  }

  def Planetar(): Fighter = {
    val damage = Dice(3, 6, 15)
    Fighter(
      name="Planetar",
      team=allyTeam,
      armorClass=32,
      hitPointsMax=Dice(17, 10, 136).Roll(),
      attacks=List[Attack](
        Attack(27, 10, damage),
        Attack(22, 10, damage),
        Attack(17, 10, damage)
      )
    )
  }

  def MovanicDeva(): Fighter = {
    val damage = Dice(3, 6, 7)
    Fighter(
      name="Movanic Deva",
      team=allyTeam,
      armorClass=24,
      hitPointsMax=Dice(12, 10, 60).Roll(),
      attacks=List[Attack](
        Attack(17, 5, damage),
        Attack(12, 5, damage),
        Attack(7, 5, damage)
      )
    )
  }

  def AstralDeva(): Fighter = {
    val damage = Dice(1, 8, 14)
    Fighter(
      name="Astral Deva",
      team=allyTeam,
      armorClass=29,
      hitPointsMax=Dice(15, 10, 90).Roll(),
      attacks=List[Attack](
        Attack(26, 5, damage),
        Attack(21, 5, damage),
        Attack(16, 5, damage)
      )
    )
  }

  def OrcWorgRider(): Fighter = {
    Fighter(
      name="Orc Worg Rider",
      team=enemyTeam,
      armorClass=18,
      hitPointsMax=Dice(2, 10, 2).Roll(),
      attacks=List[Attack](
        Attack(6, 5, Dice(1, 8, 2)),
        Attack(4, 60, Dice(1, 6))
      )
    )
  }

  def DoubleAxeFury(): Fighter = {
    val damage = Dice(1, 8, 10)
    Fighter(
      "Orc Barbarian",
      enemyTeam,
      17,
      Dice(11, 12, 45).Roll(),
      List[Attack](
        Attack(19, 5, damage),
        Attack(14, 5, damage),
        Attack(9, 5, damage)
      )
    )
  }

  def BrutalWarlord(): Fighter = {
    // http://www.d20pfsrd.com/bestiary/npc-s/npc-12/brutal-warlord-half-orc-fighter-13/
    val flail = MultiDice(List[Dice](
      Dice(1, 8, 10),
      Dice(2, 6)
    ))
    Fighter(
      "Warlord",
      enemyTeam,
      27,
      Dice(13, 10, 65).Roll(),
      List[Attack](
        Attack(24, 5, flail),
        Attack(19, 5, flail),
        Attack(14, 5, flail)
      )
    )
  }

  def GreenDragon(): Fighter = {
    // http://www.d20pfsrd.com/bestiary/npc-s/npc-12/brutal-warlord-half-orc-fighter-13/
    val bite = Dice(4, 8, 21)
    val claw = Dice(4, 6, 14)
    val wing = Dice(2, 8, 7)
    val tail = Dice(4, 6, 21)
    Fighter(
      name="Green Dragon",
      team=enemyTeam,
      armorClass=37,
      hitPointsMax=Dice(27, 12, 216).Roll(),
      attacks=List[Attack](
        Attack(33, 20, bite),
        Attack(33, 20, claw),
        Attack(33, 20, claw),
        Attack(31, 20, wing),
        Attack(31, 20, wing),
        Attack(31, 20, tail)
      )
    )
  }

  def AngelSlayer(): Fighter = {
    val angelBane = MultiDice(List[Dice](
      Dice(1, 8, 7),
      Dice(2, 6)
    ))
    Fighter(
      name="Angel Slayer",
      team=enemyTeam,
      armorClass=26,
      hitPointsMax=Dice(15, 10, 25).Roll(),
      attacks=List[Attack](
        Attack(23, 5, angelBane),
        Attack(18, 5, angelBane),
        Attack(13, 5, angelBane)
      )
    )
  }
}

case class Fighter(name : String, team : String, armorClass : Int, hitPointsMax : Int, attacks : List[Attack]) extends Serializable {
  val life = Life(hitPointsMax, ()=>{println(name.concat(" died."))})

  def IsDead(): Boolean = {
    life.CurrentHitPoints() > 0
  }
}

abstract class BaseDice() extends Serializable {
  def Roll(): Int
}

case class Dice(count: Int, faces: Int, bonus: Int = 0) extends BaseDice {

  def this(formula: String) {
    this(
      formula.split("d")(0).toInt,
      formula.split("d")(1).split("\\+")(0).toInt,
      formula.split("\\+")(1).toInt)
  }

  assert(count > 0)
  assert(faces > 0)
  val random = scala.util.Random
  override def Roll() : Int = {
    val rolls = for (i <- 1 to count) yield random.nextInt(faces) + 1
    rolls.sum + bonus
  }
}

case class MultiDice(diceGroups: List[Dice]) extends BaseDice {
  override def Roll(): Int = {
    val rolls = for(dice <- diceGroups) yield dice.Roll()
    rolls.sum
  }
}

case class Attack(touchBonus : Int, range : Int, damageDice : BaseDice) extends Serializable {
  val attackDie = Dice(1, 20, touchBonus)

  def this(touchBonus: Int, range: Int, damageFormula: String) {
    this( touchBonus, range, new Dice(damageFormula))
  }

  def RollAgainst(armorClass: Int) : Int = {
    val roll = attackDie.Roll()
    if(roll > armorClass || roll - attackDie.bonus == 20)
      damageDice.Roll()
    else
      0
  }
}

case class Life(maxHitPoints: Int, onDeath : () => Unit) extends Serializable {
  val hitPointsHistory = ArrayBuffer(maxHitPoints)

  def CurrentHitPoints() : Int = {
    hitPointsHistory.sum
  }

  def Hurt(damage: Int): Unit = {
    hitPointsHistory += -damage
    if (CurrentHitPoints() <= 0)
      onDeath()
  }

  def Heal(healing: Int): Unit = {
    hitPointsHistory += healing
    val currentHP = CurrentHitPoints()
    if (currentHP > maxHitPoints)
      hitPointsHistory += maxHitPoints - currentHP
  }
}
