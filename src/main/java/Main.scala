
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import parser._
import fighters._
import scala.collection.mutable.ArrayBuffer

import org.apache.spark.graphx._


object SimpleScalaSpark extends App {

  override def main(args: Array[String]) {
    // Create Spark Context
    val conf = new SparkConf().setAppName("PitoTheSpellCaster").setMaster("local[*]")
    val spark = new SparkContext(conf)
    spark.setLogLevel("ERROR")

    // EXERCICE 1
    def SortCreaturesBySpell(): Unit = {
      // Create RDD
      val source: String = scala.io.Source.fromFile("../Scraper/creatures.json").getLines.mkString
      val creatures: List[Creature] = new DNDJsonParser(source).ParseSpellCasters()
      val rdd = spark.parallelize(creatures)
      println("SPELLS BY CREATURE")
      rdd.foreach((c) => println(c))

      // Map Reduce
      val spellMap = rdd.flatMap((c) => c.spells.map((spell) => (spell, ArrayBuffer(c.name))))
      println("MAP (creatures by spell)")
      spellMap.foreach(println(_))
      val creaturesBySpell = spellMap.reduceByKey((a, b) => a ++ b)
      println("REDUCE (creatures by spell)")
      creaturesBySpell.foreach(println(_))
    }

    // EXERCICE #2
    def SmallFight(): Unit = {
      val firstBattle = new Battle()

      firstBattle.AddFighter(Bestiary.Solar())
      firstBattle.AddFighter(Bestiary.BrutalWarlord())
      for (i <- 1 to 4) firstBattle.AddFighter(Bestiary.DoubleAxeFury())
      for (i <- 1 to 9) firstBattle.AddFighter(Bestiary.OrcWorgRider())

      val vectors = firstBattle.GetVerticesAndEdges()
      firstBattle.RollInitiative(spark.makeRDD(vectors._1), spark.makeRDD(vectors._2))

      println("START FIGHT")
      while (firstBattle.PlayRound()) {
        println("END ROUND")
      }
      println("END FIGHT")
    }

    def HugeBattle(): Unit = {
      val firstBattle = new Battle()

      // Allies
      firstBattle.AddFighter(Bestiary.Solar())
      for (i <- 1 to 2) firstBattle.AddFighter(Bestiary.Planetar())
      for (i <- 1 to 2) firstBattle.AddFighter(Bestiary.MovanicDeva())
      for (i <- 1 to 5) firstBattle.AddFighter(Bestiary.AstralDeva())

      // Enemies
      firstBattle.AddFighter(Bestiary.GreenDragon())
      for (i <- 1 to 200) firstBattle.AddFighter(Bestiary.DoubleAxeFury())
      for (i <- 1 to 10) firstBattle.AddFighter(Bestiary.AngelSlayer())

      val vectors = firstBattle.GetVerticesAndEdges()
      firstBattle.RollInitiative(spark.makeRDD(vectors._1), spark.makeRDD(vectors._2))

      println("START FIGHT")
      while (firstBattle.PlayRound()) {
        println("END ROUND")
      }
      println("END FIGHT")
    }

    SortCreaturesBySpell()
    SmallFight()
    HugeBattle()
  }
}
